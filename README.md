# O impacto do Bolsonarismo sobre a cobertura vacinal contra a covid-19 nos municípios brasileiros


### Objetivo do repositório:

Disponibilizar os bancos de dados e os scripts de tratamento e análises do presente artigo.


[https://preprints.scielo.org/index.php/scielo/preprint/view/5027]


### Autores:


**Vitor de Moraes Peixoto**

Universidade Estadual do Norte Fluminense Darcy Ribeiro (UENF)
Orcid: [https://orcid.org/0000-0001-6618-3311]


**João Gabriel Ribeiro Pessanha Leal**

ENSP/FIOCRUZ
Orcid: [https://orcid.org/0000-0003-4851-1435]


**Larissa Martins Marques**

IPOL/Universidade de Brasília (UnB).
Orcid: [https://orcid.org/0000-0003-1618-1742]




### Abstract

The immunization campaign against COVID-19 started in Brazil in January 2021 after strong pressure from society on the federal government, which had created a series of ideological obstacles against vaccines, especially those produced with Chinese inputs. This article analyzes the impact of far-right ideology on the spatial distribution of vaccine coverage against Covid-19 in Brazilian municipalities. By means of hierarchical models, it was identified that, maintaining constant socio-demographic characteristics and the structures of the Unified Health System, the degree of Bolsonarism in the municipalities had a negative impact on the coverage rates of the first, second and, especially, of the third dose.
keywords
Vaccination coverage; Immunization campaigns; Bolsonarism; far right; General elections; Brazilian Municipalities.

#### Resumo 
A campanha de imunização contra a Covid-19 foi iniciada no Brasil em Janeiro de 2021 após forte pressão da sociedade sobre o governo federal, que havia criado uma série de empecilhos ideológicos às vacinas, sobretudo as produzidas com insumos chineses. Este artigo analisa o impacto da ideologia de extrema direita na distribuição espacial da cobertura vacinal contra Covid-19 nos municípios brasileiros. Por meio de modelos hierárquicos multiníveis de dois estágios identificou-se que, mantidas constantes as características sociodemográficas e as estruturas do Sistema Único de Saúde, o grau de bolsonarismo nos municípios impactou negativamente as taxas de cobertura da primeira, da segunda e, especialmente, da terceira dose da vacina.

### Palavras-chave

Cobertura vacinal; Campanhas de imunização; Bolsonarismo; Extrema direita; Eleições gerais; Municípios Brasileiros.



### Contributors
All authors contributed to the conception and design of the study, analysis and interpretation of the data, and writing of the article; approved the final version to be published; and are responsible for all aspects of the work in ensuring the accuracy and completeness of any part of the work.
