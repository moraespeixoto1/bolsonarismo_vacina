#############################################################

#        Script para resultados finais do artigo            #

#############################################################

# 1. Abrir pacotes ----

pacman::p_load(tidyverse, lubridate, ggthemes, scales, lme4, sjPlot, performance,
               geobr, ggpmisc, grid, patchwork, equatiomatic)


# 2. Abrir bancos e definir tema ----

load("Bancos/indicadores_datasus.Rda")

load("Bancos/datasus_eleicao_2022.Rda")

## 2.1 Definir tema ----

tema <- theme_fivethirtyeight(base_family = "Times New Roman") +
  theme(plot.title = element_text(size = 35, margin = margin(0, 0, 30, 0)),
        axis.title.x = element_text(size = 28),
        axis.title.y = element_text(size = 28),
        axis.text.x =  element_text(size = 28),
        axis.text.y =  element_text(size = 28),
        legend.position = "bottom",
        legend.text = element_text(size = 26),
        plot.caption = element_text(hjust = 0, size = 14),
        panel.background = element_rect(fill = "white", colour = "white", color = "white"),
        plot.background = element_rect(fill = "white", colour = "white", color = "white"),
        legend.background = element_rect(fill = "white"),
        panel.grid.major.x = element_line(colour = "#eceff2", size = 0.8),
        panel.grid.major.y = element_line(colour = "#eceff2", size = 0.8),
        legend.box = "horizontal",
        legend.key = element_rect(fill = "white", color = "white"),
        #strip.background = element_rect(fill = "white", colour = "white"),
        legend.spacing.x = unit(1.0, "cm"),
         legend.key.width = unit(2, "cm"))

ggplot2::theme_set(tema)


# 3.0 Resultados ----

## 3.1 diagrama primeira dose Bolsonaro ----

datasus_eleicao_2022 %>% 
  filter(perc_ao_menos_uma_dose <= 100) %>% 
  ggplot()+
  aes(x = Partido_22, y = perc_ao_menos_uma_dose)+
  geom_point()+
  geom_smooth(method = "lm")+
  facet_grid(regiao~Faixa_pop)+
  stat_poly_eq(aes(label = after_stat(eq.label))) +
  stat_poly_eq(label.y = 0.7) 


## 3.2 diagrama segunda dose Bolsonaro ----

datasus_eleicao_2022 %>% 
  filter(perc_ao_menos_duas_doses <= 100) %>% 
  ggplot()+
  aes(x = Partido_22, y = perc_ao_menos_duas_doses)+
  geom_point()+
  geom_smooth(method = "lm")+
  facet_grid(regiao~Faixa_pop)+
  stat_poly_eq(aes(label = after_stat(eq.label))) +
  stat_poly_eq(label.y = 0.7) 

## 3.3 diagrama terceira dose Bolsonaro ----

(graf_1 <- datasus_eleicao_2022 %>% 
  mutate(Faixa_pop_cat = case_when(Faixa_pop == 1 ~ "< 5 mil",
                               Faixa_pop == 2 ~ "> 5 e < 10 mil",
                               Faixa_pop == 3 ~ "> 10 e < 20 mil",
                               Faixa_pop == 4 ~ "> 20 e < 50 mil",
                               Faixa_pop == 5 ~ "> 50 e < 100 mil",
                               Faixa_pop == 6 ~ "> 100 e < 500 mil",
                               Faixa_pop == 7 ~ "> 500 mil")) %>% 
  filter(perc_ao_menos_terceira_dose <= 100) %>% 
  ggplot()+
  aes(x = Partido_22, y = perc_ao_menos_terceira_dose)+
  geom_point()+
  geom_smooth(method = "lm", size=3, 
              se= FALSE)+
  facet_grid(regiao~ fct_reorder(Faixa_pop_cat, Faixa_pop))+
  ggpubr::stat_regline_equation(aes(label=paste(..rr.label.., sep = "\\")), size = 7.3) +
  scale_y_continuous(labels=function(x) paste0(x,"%"))+
  ylab("Cobertura com ao menos uma dose de reforço") + xlab("Bolsonarismo")+
  theme(axis.title.x = element_text(size = 34, family = "Times New Roman"),
        axis.title.y = element_text(size = 34, family = "Times New Roman"),
        axis.text.y = element_text(size = 24, family = "Times New Roman"),
        axis.text.x = element_text(size = 31, family = "Times New Roman"),
        strip.text = element_text(size = 31.5, family = "Times New Roman"), 
        panel.spacing.y = unit(1.7, "lines")))

### salvar o grafico

ggsave(graf_1, height = 50, width = 65, dpi = 250, units = "cm",
      filename = "graf_1.png")


## 3.4 modelos hierarquicos ----

# filtrar os casos de municipios com mais de 120% de vacinacao em cada banco

### 3.3.1 Modelo ao menos uma dose

modelo_bolsonaro_primeira_dose <- lmer(perc_ao_menos_uma_dose ~
                                         Partido_22 +
                                         gini_2010 +
                                         pib_per_capita_2017 +
                                         equipamentos_saude_proporcionado +
                                         muni_polo_saude +
                                         log(populacao_2021)+
                                         percentual_mais_60+
                                         (1|uf),
                                         data = subset(datasus_eleicao_2022, perc_ao_menos_uma_dose<=120))


### 3.3.2 Modelo ao menos duas doses


modelo_bolsonaro_segunda_dose <- lmer(perc_ao_menos_duas_doses ~
                                         Partido_22 +
                                         gini_2010 +
                                         pib_per_capita_2017 +
                                         equipamentos_saude_proporcionado +
                                         muni_polo_saude +
                                         log(populacao_2021)+
                                         percentual_mais_60+
                                         (1|uf),
                                         data = subset(datasus_eleicao_2022,perc_ao_menos_duas_doses<=120 ))



### 3.3.3 Modelo ao menos terceira dose


modelo_bolsonaro_terceira_dose <- lmer(perc_ao_menos_terceira_dose ~
                                        Partido_22 +
                                        gini_2010 +
                                        pib_per_capita_2017 +
                                        equipamentos_saude_proporcionado +
                                        muni_polo_saude +
                                        log(populacao_2021)+
                                        percentual_mais_60+
                                        (1|uf),
                                        data = subset(datasus_eleicao_2022, perc_ao_menos_terceira_dose <= 120))

# visualizar os modelos em uma tabela

tab_model(modelo_bolsonaro_primeira_dose, 
          modelo_bolsonaro_segunda_dose, 
          modelo_bolsonaro_terceira_dose)



## 3.5 Performance dos modelos hierarquicos ----

check_model(modelo_bolsonaro_primeira_dose)
check_model(modelo_bolsonaro_segunda_dose)
check_model(modelo_bolsonaro_terceira_dose)

## 3.6 equacao do modelo 

extract_eq(modelo_bolsonaro_primeira_dose, wrap = TRUE, terms_per_line = 2)

## 3.6 Efeitos esperados ---- 


(plot_1 <- plot_model(modelo_bolsonaro_primeira_dose, type = "pred",
                     terms = c("Partido_22", "muni_polo_saude")) + 
  labs(title = "Ao menos uma dose",
    x = "Bolsonarismo", 
    y = "Taxa de vacinação",
    col = NULL)+
  scale_color_manual(values = c("#0080ff", "#003300"), 
                       labels = c("Não é pólo","É pólo"))+
  scale_x_continuous(labels=function(x) paste0(x,"%"))+
  scale_fill_manual(values = c("#0080ff", "#003300"))+
  scale_y_continuous(limits = c(20,95), 
                      breaks = c(20, 30, 40, 50, 60, 70, 80, 90, 100), 
                      labels=function(x) paste0(x,"%")))


(plot_2 <- plot_model(modelo_bolsonaro_segunda_dose, type = "pred",
                      terms = c("Partido_22", "muni_polo_saude"))+ 
    labs(title = "Ao menos duas doses",
         x = "Bolsonarismo", 
         col = NULL, 
         y = NULL)+
  scale_x_continuous(labels=function(x) paste0(x,"%"))+
  scale_color_manual(values = c("#0080ff", "#003300"), 
                     labels = c("Não é pólo","É pólo"))+
  scale_fill_manual(values = c("#0080ff", "#003300"))+
  scale_y_continuous(limits = c(20,95), 
                       breaks = c(20, 30, 40, 50, 60, 70, 80, 90, 100),
                       labels=function(x) paste0(x,"%")))


(plot_3 <- plot_model(modelo_bolsonaro_terceira_dose, type = "pred",
                      terms = c("Partido_22", "muni_polo_saude"))+
    labs(title = "Ao menos três doses",
         x = "Bolsonarismo", 
         col = NULL, 
         y = NULL)+
    scale_color_manual(values = c("#0080ff", "#003300"), 
                       labels = c("Não é pólo","É pólo"))+
    scale_x_continuous(labels=function(x) paste0(x,"%"))+
  scale_fill_manual(values = c("#0080ff", "#003300"))+
  scale_y_continuous(limits = c(20,95), 
                     breaks = c(20, 30, 40, 50, 60, 70, 80, 90, 100),
                     labels=function(x) paste0(x,"%")))
  
### 3.6.1 juntar todos os graficos de efeitos esperados e salvar a imagem

plot_todos <- plot_1 + plot_2 + plot_3

ggsave(plot_todos, height = 32, width = 64, dpi = 250, units = "cm",
       filename = "plot_prob_predita_unificado_y.png")


############## GRAFICOS DOS EFEITOS DOS ESTADOS ############################

plot_model(modelo_bolsonaro_primeira_dose, type = "re")

plot_model(modelo_bolsonaro_segunda_dose, type = "re")

plot_model(modelo_bolsonaro_terceira_dose, type = "re")

########################################################################

## 3.7 Mapas ----

mun <- read_municipality(code_muni = "all")

mun_datasus <- left_join(mun, datasus_eleicao_2022,
                         by = c("code_muni" = "id_municipio"))

estados <- read_state(code_state = "all")

(vacina <-mun_datasus %>% 
  filter(perc_ao_menos_terceira_dose <= 120) %>% 
  ggplot()+
  geom_sf(aes(fill = perc_ao_menos_terceira_dose), color = NA)+
  geom_sf(data = estados, alpha = 0.01)+
  labs(fill = NULL, 
       col = NULL, 
       title = "Taxa de vacinação (3ª dose)")+
  ggspatial::annotation_scale()+
  scale_fill_continuous(high = "#001a33", low = "#b3daff")+
  ggspatial::annotation_north_arrow(
    location = "br",
    which_north = "true",
    height = unit(1, "cm"),
    width = unit(1, "cm"),
    pad_x = unit(0.1, "in"),
    pad_y = unit(0.1, "in"),
    style = ggspatial::north_arrow_fancy_orienteering)+
  theme(axis.title.x = element_text(size = 15),
        axis.title.y = element_text(size = 15),
        axis.text.y = element_text(size = 15),
        axis.text.x = element_text(size = 15),
        legend.key.width = unit(0.7, "cm")))

# cor alternativa
#high = "#003300", low = "#e6ffe6"

(bolsonaro22 <- mun_datasus %>% 
  ggplot()+
  geom_sf(aes(fill = Partido_22), color = NA)+
  geom_sf(data = estados, alpha = 0.01)+
  ggspatial::annotation_scale()+
  labs(fill = NULL, 
         col = NULL, 
       title = "Voto em Bolsonaro 1º turno (2022)")+
  scale_fill_continuous(high = "#003300", low = "#e6ffe6")+
    ggspatial::annotation_north_arrow(
    location = "br",
    which_north = "true",
    height = unit(1, "cm"),
    width = unit(1, "cm"),
    pad_x = unit(0.1, "in"),
    pad_y = unit(0.1, "in"),
    style = ggspatial::north_arrow_fancy_orienteering)+
    theme(axis.title.x = element_text(size = 15),
          axis.title.y = element_text(size = 15),
          axis.text.y = element_text(size = 15),
          axis.text.x = element_text(size = 15),
          legend.key.width = unit(0.7, "cm")))


### 3.7.1 juntar todos os mapas e salvar a imagem

(mapa <- bolsonaro22 + vacina)

ggsave(mapa, height = 22, width = 45, dpi = 250, units = "cm",
       filename = "mapa.png")
